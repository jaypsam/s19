//alert("Hello Joshua");

/*
	Selection Control Structures
		- it sorts out whether the statement/s are to be executed based on the condition
		- two-way selection (true or false)
		-multi-way selection
*/

//IF ELSE STATEMENTS

/*
	Syntax:
		if(control){
			//statement
		} else if (condition){
			//statement
		} else {
			//statement
		}
*/

/*
	if statement - execute a statement if a specified condition is true
*/

let numA = -1;

if (numA < 0){
	console.log("Hello")
}

console.log(numA < 0);

//String

let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City");
}

/*
	Else if
		- executes a statement if our previous condition are false and if the specified condition is true
		- the "else if" clause is option additional conditions to change the flow of a program
*/

let numB = 1

if(numA > 0){
	console.log("Hello");
} else if (numB > 0){
	console.log("World");
}

city = "Tokyo";

if(city === "New York"){
	console.log("Welcom to New York City");
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo");
}

/*
	else statement
		- execute a statement if all of our previous conditions are false
*/



if(numA > 0){
	console.log("Hello");
} else if (numB === 0){
	console.log("World");
} else{
	console.log("Again")
}

let age = parseInt(prompt("Enter your age: "));

if(age <= 18){
	console.log("Not allowed to drink");
} else{
	console.log("Congrats! You can now take a shot");
}


/*
let height = parseInt(prompt("Enter your height: "));
if(height <= 150){
	console.log("Did not pass the min height requirement");
} else{
	console.log("Pass the min requirements")
}
*/
function minHeight(height){
	if(height <= 150){
		console.log("Did not pass the minimum requirement.");
	} else if(height > 150){
		console.log("Passed the minimum requirements");
	}
}

minHeight(139);

//Nested if statement

let isLegalAge = true;

let isAdmin = false;

if(isLegalAge){
	if(!isAdmin){
		console.log("you are not an admin!");
	}
}

let message = "No Message";

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'Not a typhoon';
	} else if (windSpeed <= 61){
		return 'Tropical depression detected';
	} else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';
	} else if(windSpeed >= 89 && windSpeed <= 177){
		return 'Severe tropical storm detected';
	} else{
		return 'typhoon detected';
	}
}

message = determineTyphoonIntensity(70)
console.log(message);

//Truthy and Falsy

/*
	In JS a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy values:
		1. false
		2. 0
		3. -0
		4. null
		6. undefined
		7. NaN
*/

//Truthy examples:

let word = "true";

if(word){
	console.log("Truthy");
}

if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if([]){
	console.log("Truthy");
}

//Falsy examples:

if(false){
	console.log("Falsy");
}

if(0){
	console.log("Falsy");
}

if(-0){
	console.log("Falsy");
}

if(undefined){
	console.log("Falsy");
}

if (null){
	console.log("Falsy");
}
/*
if(Nan);{
	console.log("Falsy");
}
*/

//Conditional (Ternary) Operator - for short codes
/*
	Syntax:
		(condition)? ifTrue+expression;
*/

//Single statement execution
let ternaryResult = (1 < 18)? "Condition is True" : "Condition is False";
console.log("Result of ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name = 'Joshua';
	return 'You are legal age';
}

function isUnderAge(){
	name = 'Emman';
	return 'You are under the age limit';
}

let yourAge = parseInt(prompt("What is your age?"));

let
legalAge = (age > 18)? isOfLegalAge() : isUnderAge();

console.log("Result of Ternary Operator in Function: " + legalAge + ',' + name);

//Switch Statement
/*
	Can be used as alternative to an if ... else statement where the data to be used in the condition of an expected input:

	Syntax:

		switch (expression){
			case <value>:
			statement;
			break;
		default:
			statement;
			break;	
		}
*/

let day = prompt("What day of the week is it today?").toLowerCase();

console.log(day);

switch(day){
	case 'monday':
	console.log("The color of the day is red.");
	break;
	case 'tuesday':
	console.log("The color of the day is orange.");
	break;
	case 'wednesday':
	console.log("The color of the day is yellow.");
	break;
	case 'thursday':
	console.log("The color of the day is green.");
	break;
	case 'friday':
	console.log("The color of the day is blue.");
	break;
	case 'saturday':
	console.log("The color of the day is indigo.");
	break;
	case 'sunday':
	console.log("The color of the day is violet.");
	break;	
	default:
		console.log("Input a valid day");
		break;					
}

//break; - is for ending the loop
//switch - is not very popular statement in coding.

//Try-Catch-Finally Statement

/*
	-try-catch is commonly used for error handling.
	-will still function even if the statement is not complete
*/

function showIntensityAlert(windSpeed){
	try{
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		console.log(typeof error);
		console.log(error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert!");
	}
}

showIntensityAlert(56);





































