alert("Welcome to Zuitt");

let userName;
let userPassword;
let userRole;

function login() {
  userName = prompt("Username");
  userPassword = prompt("Password");
  userRole = prompt("Role");

console.log("Your username: ");
console.log(userName);
console.log("Role: ");
console.log(userRole);

  if (!userName || !userPassword || !userRole) {
    alert("All inputs should not be empty!");
  } else {
    switch (userRole) {
      case "admin":
        alert("Welcome back to the class portal, " + userRole + "!");
        break;
      case "teacher":
        alert("Thank you for logging in, " + userRole + "!");
        break;

      case "student":
        alert("Welcome to the class portal, " + userRole + "!");
        break;

      default:
        alert("Role out of range.");
        break;
    }
  }
}

login();

function getAverage(...numbers){
    let x = 1;
    let secondary = 0;

    for(const number of numbers){
        secondary += number;
        x++;
    }

     let average = Math.round(secondary / (x - 1));
    console.log(average);

    if(average <= 74){
        console.log("Hello, " + userName + " student, your average is " +  average + ". The letter equivalent is F");
    }else if(average >= 75 && average <= 79){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is D");

    }else if(average >= 80 && average <= 84){
        console.log("Hello, " +userName+ " student, your average is " +  average + ". The letter equivalent is C");

    }else if(average >= 85 && average <= 89){
        console.log("Hello, " +userName+ " student, your average is " +  average + ". The letter equivalent is B");

    }else if(average >= 90 && average <= 95){
        console.log("Hello, " +userName+ " student, your average is " +  average + ". The letter equivalent is A");

    }else {
        console.log("Hello, " +userName+ " student, your average is " +  average + ". The letter equivalent is A+");
    }
}

getAverage(80,90,95,99);

